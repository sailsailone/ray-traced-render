import time
import taichi as ti
from taichi.math import vec2
from taichi.ui import LEFT, RIGHT, UP, DOWN, RELEASE

from .fileds import image_pixels, diff_pixels, ray_buffer
from .camera import smooth, camera_exposure, camera_focus, camera_aperture, camera_vfov
from .scene import build_scene, OBJECTS, objects
from .renderer import render

window = ti.ui.Window("渲染引擎", (1920 * 4 // 9, 1080 * 4 // 9), vsync=True)
canvas = window.get_canvas()
camera = ti.ui.Camera()
camera.position(0, -0.2, 4.0)
smooth.init(camera)
GRAVITY = [0, -9.8, 0]
curr_preset_id = 0
curr_preset_id = False


def show_options():
    global curr_preset_id
    global OBJECTS
    with gui.sub_window("New Object or HDR", 0, 0.4, 0.13, 0.4) as w:  # 新增物体或更换HDR图片
        a = w.checkbox("OBJ", curr_preset_id)
        w.checkbox("HDR", False)
        GRAVITY[0] = w.slider_float("x", GRAVITY[0], -10, 10)

    with gui.sub_window("Camera Parameters", 0.87, 0, 0.13, 0.2) as w:  # 调整相机参数
        OBJECTS[1].transform.position.y= w.slider_float("ratio", OBJECTS[1].transform.position.y, -10, 10)
        GRAVITY[1] = w.slider_float("expos", GRAVITY[1], -10, 10)
        GRAVITY[2] = w.slider_float("vfov", GRAVITY[2], -10, 10)
    with gui.sub_window("List of Objects", 0, 0, 0.15, 0.7) as w:
        old_preset = curr_preset_id
        for i in range(len(OBJECTS)):
            if OBJECTS[i].type == 1:
                object_name = "SPHERE"
            elif OBJECTS[i].type == 2:
                object_name = "BOX"
            elif OBJECTS[i].type == 3:
                object_name = "CYLINDER"
            elif OBJECTS[i].type == 4:
                object_name = "CONE"
            elif OBJECTS[i].type == 5:
                object_name = "PLANE"
            elif OBJECTS[i].type == 6:
                object_name = "BUNNY"
            if w.checkbox(object_name, curr_preset_id == i):
                curr_preset_id = i
                w.text("Transform")
                OBJECTS[i].transform.position.x= w.slider_float("px", OBJECTS[
                    curr_preset_id].transform.position.x, -10, 10)
                OBJECTS[i].transform.position.y = w.slider_float("py", OBJECTS[
                    curr_preset_id].transform.position.y, -10, 10)
                OBJECTS[i].transform.position.z = w.slider_float("pz", OBJECTS[
                    curr_preset_id].transform.position.z, -10, 10)
                OBJECTS[i].transform.rotation.x = w.slider_float("rx", OBJECTS[
                    curr_preset_id].transform.rotation.x, -10, 10)
                OBJECTS[i].transform.rotation.y = w.slider_float("ry", OBJECTS[
                    curr_preset_id].transform.rotation.y, -10, 10)
                OBJECTS[i].transform.rotation.z = w.slider_float("rz", OBJECTS[
                    curr_preset_id].transform.rotation.z, -10, 10)
                OBJECTS[i].transform.scale.x = w.slider_float("scale", OBJECTS[
                    curr_preset_id].transform.scale.z, 0, 100)
                OBJECTS[i].transform.scale.y=OBJECTS[i].transform.scale.x
                OBJECTS[i].transform.scale.z=OBJECTS[i].transform.scale.x
                w.text("Material")


build_scene()  # 光追场景渲染

prev_time = time.time()
gui = window.get_gui()

while window.running:
    # 子窗口
    curr_time = time.time()
    dt = curr_time - prev_time  # dt 表示两次渲染循环之间的时间差
    prev_time = curr_time

    # 相机移动方向
    direction = vec2(float(window.is_pressed(RIGHT)) - float(window.is_pressed(LEFT)),
                     float(window.is_pressed(UP)) - float(window.is_pressed(DOWN)))

    refreshing = False  # 根据用户按下的按键来调整相机的各种参数，并在窗口中实时更新相机视角。
    if window.is_pressed('z'):  # 按下 'z' 键时，会将相机视场角的值增加 direction.y * dt * 10
        camera_vfov[None] += direction.y * dt * 10  # 其中 direction.y 表示用户按下 'up' 或 'down' 键的方向
        direction.y = 0  # 将 direction.y 设为 0 ，并将 refreshing 设为 True 。
        refreshing = True  # 在控制台中输出相机视场角的值
        print('vfov', camera_vfov[None])
    elif window.is_pressed('x'):
        camera_aperture[None] += direction.y * dt  # 将相机光圈的值增加 direction.y * dt
        direction.y = 0
        refreshing = True
        print('aperture', camera_aperture[None])
    elif window.is_pressed('c'):
        camera_focus[None] += direction.y * dt  # 将相机聚焦距离的值增加 direction.y * dt
        direction.y = 0
        refreshing = True
        print('focus', camera_focus[None])  # 最后在控制台中输出相机聚焦距离的值
    elif window.is_pressed('v'):
        camera_exposure[None] += direction.y * dt  # 会将相机曝光值的值增加 direction.y * dt
        direction.y = 0
        print('exposure', camera_exposure[None])  # 在控制台中输出相机曝光值的值
    for event in window.get_events(RELEASE):
        if event.key == 'g':
            ti.tools.imwrite(image_pixels, 'out/main_' +
                             str(curr_time) + '.png')
    speed = dt * 5 * (10 if window.is_pressed('Shift') else 1)
    camera.track_user_inputs(window, movement_speed=speed, hold_key=ti.ui.LMB)
    smooth.update(dt, camera, direction)
    render(refreshing)  # refreshing 变量的作用是判断是否需要重新渲染画面，如果用户更改了相机参数，则需要重新渲染画面并更新窗口中的图像。
    canvas.set_image(image_pixels)
    # show_options()

    window.show()
