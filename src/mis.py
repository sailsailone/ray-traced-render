import taichi as ti
import math

from taichi.math import reflect


@ti.func
def toNormalHemisphere(v, N):
    helper = ti.Vector([1.0, 0.0, 0.0])
    if abs(N[0]) > 0.999:
        helper = ti.Vector([0.0, 0.0, 1.0])
    tangent = ti.normalized(ti.cross(N, helper))
    bitangent = ti.normalized(ti.cross(N, tangent))
    return v[0] * tangent + v[1] * bitangent + v[2] * N

@ti.func
def SampleCosineHemisphere(xi_1, xi_2, N):
    r = ti.sqrt(xi_1)
    theta = xi_2 * 2.0 * math.pi
    x = r * ti.cos(theta)
    y = r * ti.sin(theta)
    z = ti.sqrt(1.0 - x*x - y*y)

    L = toNormalHemisphere(ti.Vector([x, y, z]), N)
    return L


import taichi as ti

@ti.func
def SampleGTR2(xi_1, xi_2, V, N, alpha):
    phi_h = 2.0 * ti.PI * xi_1
    sin_phi_h = ti.sin(phi_h)
    cos_phi_h = ti.cos(phi_h)

    cos_theta_h = ti.sqrt((1.0-xi_2)/(1.0+(alpha*alpha-1.0)*xi_2))
    sin_theta_h = ti.sqrt(ti.max(0.0, 1.0 - cos_theta_h * cos_theta_h))

    # 采样 "微平面" 的法向量 作为镜面反射的半角向量 h
    H = ti.Vector([sin_theta_h*cos_phi_h, sin_theta_h*sin_phi_h, cos_theta_h])
    H = toNormalHemisphere(H, N)   # 投影到真正的法向半球

    # 根据 "微法线" 计算反射光方向
    L = reflect(-V, H)
    return L

@ti.func
def SampleBRDF(xi_1, xi_2, xi_3, V, N, material):
    alpha_GTR1 = ti.mix(0.1, 0.001, material.clearcoatGloss)
    alpha_GTR2 = ti.max(0.001, material.roughness ** 2)

    # 辐射度统计
    r_diffuse = (1.0 - material.metallic)
    r_specular = 1.0
    r_clearcoat = 0.25 * material.clearcoat
    r_sum = r_diffuse + r_specular + r_clearcoat

    # 根据辐射度计算概率
    p_diffuse = r_diffuse / r_sum
    p_specular = r_specular / r_sum
    p_clearcoat = r_clearcoat / r_sum

    # 按照概率采样
    rd = xi_3

    # 漫反射
    if rd <= p_diffuse:
        return SampleCosineHemisphere(xi_1, xi_2, N)
    # 镜面反射
    elif p_diffuse < rd and rd <= p_diffuse + p_specular:
        return SampleGTR2(xi_1, xi_2, V, N, alpha_GTR2)
    # 清漆
    elif p_diffuse + p_specular < rd:
        return SampleGTR2(xi_1, xi_2, V, N, alpha_GTR1)
    return ti.Vector([0.0, 1.0, 0.0])


import taichi as ti


@ti.func
def BRDF_Pdf(V: ti.template(), N: ti.template(), L: ti.template(), material: ti.template()) -> float:
    NdotL = N.dot(L)
    NdotV = N.dot(V)
    if NdotL < 0 or NdotV < 0:
        return 0.0

    H = (L + V).normalized()
    NdotH = N.dot(H)
    LdotH = L.dot(H)

    # 镜面反射 -- 各向同性
    alpha = max(0.001, material.roughness ** 2)
    Ds = SampleGTR2(NdotH, alpha)
    Dr = SampleGTR2(NdotH, ti.mix(0.1, 0.001, material.clearcoatGloss))  # 清漆

    # 分别计算三种 BRDF 的概率密度
    pdf_diffuse = NdotL / ti.pi
    pdf_specular = Ds * NdotH / (4.0 * LdotH)
    pdf_clearcoat = Dr * NdotH / (4.0 * LdotH)

    # 辐射度统计
    r_diffuse = (1.0 - material.metallic)
    r_specular = 1.0
    r_clearcoat = 0.25 * material.clearcoat
    r_sum = r_diffuse + r_specular + r_clearcoat

    # 根据辐射度计算选择某种采样方式的概率
    p_diffuse = r_diffuse / r_sum
    p_specular = r_specular / r_sum
    p_clearcoat = r_clearcoat / r_sum

    # 根据概率混合 pdf
    pdf = p_diffuse * pdf_diffuse \
          + p_specular * pdf_specular \
          + p_clearcoat * pdf_clearcoat

    pdf = max(1e-10, pdf)
    return pdf


@ti.func
def misMixWeight(a: ti.f32, b: ti.f32) -> ti.f32:
    t = a * a
    return t / (b*b + t)