import taichi as ti  # of course, we need taichi
from taichi import float32
from taichi.math import *  # need common mathematical operations
import taichi as ti
import meshtaichi_patcher as Patcher

ti.init(arch=ti.gpu, default_ip=ti.i32, default_fp=ti.f32)  # initialize, use GPU, set default ip and fp

image_resolution = (512, 512)  # resolution of the image, not too large
image_buffer = ti.Vector.field(4, float, image_resolution)  # image buffer field for recording sample counts
image_pixels = ti.Vector.field(3, float, image_resolution)  # for output display pixels to the screen
Ray = ti.types.struct(origin=vec3,
                      direction=vec3,
                      color=vec3)  # the struct representing camera light ray
Material = ti.types.struct(albedo=vec3,
                           emission=vec3)  # Cornell Box need only albedo and emission
Transform = ti.types.struct(position=vec3,
                            rotation=vec3,
                            scale=vec3)  # Transformation of SDF objects
SDFObject = ti.types.struct(distance=float,
                            transform=Transform,
                            material=Material)  # SDF objects
Tri = ti.types.struct(v1=vec3,
                      v2=vec3,
                      v3=vec3,
                      material=Material)
Triangles = [  # 场景scene中的物体列表
    Tri(vec3(0.1102022, 0.74010998, 1.13239801),
        vec3(-1.57671404, 1.97363997, -0.0373716),
        vec3(-1.27691603, -0.26664999, 0.84513003),
        Material(vec3(1,1 , 1)*0.4, vec3(100))),
    Tri(vec3(0.16808981, -0.27832401, -0.459728 ),
        vec3(1.11901402, 0.016062, 0.1041062),
        vec3(0.210986, 1.44327998, -0.23300999),
        Material(vec3(1, 1, 0)*0.4, vec3(100))),
    # Tri(vec3(1, 1, 1), vec3(-1, 1, 1), vec3(-1, 1, -1),
    #     Material(vec3(1, 1, 1)*0.4, vec3(1))),
    # Tri(vec3(1, 1, 1), vec3(-1, 1, -1), vec3(1, 1, -1),
    #     Material(vec3(1, 1, 1)*0.4, vec3(1))),
    # Tri(vec3(0.4, 0.99, 0.4), vec3(-0.4, 0.99, -0.4), vec3(-0.4, 0.99, 0.4),
    #     Material(vec3(1, 1, 1) * 0.4, vec3(100))),
    # Tri(vec3(0.4, 0.99, 0.4), vec3(0.4, 0.99, -0.4), vec3(-0.4, 0.99, -0.4),
    #     Material(vec3(1, 1, 1) * 0.4, vec3(100))),
    # Tri(vec3(0, -0.2, -0.95), vec3(0.2, 0.5, -0.95), vec3(-0.0, -0.9, 0.4),Material(vec3(1, 1, 1)*0.4, vec3(1))),
    # Tri(vec3(0.2, 0.98, 0.2), vec3(-0.2, 0.98, -0.2), vec3(0.2, 0.98, -0.2),
    #     Material(vec3(1, 1, 1) * 0.4, vec3(100))),
    # Tri(vec3(1, -1, -1), vec3(-1, 1, -1), vec3(-1, -1, -1),
    #     Material(vec3(1, 1, 1)*0.4, vec3(1))),
    # Tri(vec3(1, -1, -1), vec3(1, 1, -1), vec3(-1, 1, -1),Material(vec3(1, 1, 1)*0.4, vec3(1))),
    # Tri(vec3(-1, -1, -1), vec3(-1, 1, 1), vec3(-1, -1, 1),Material(vec3(1, 0, 0)*0.5, vec3(1))),
    # Tri(vec3(-1, -1, -1), vec3(-1, 1, -1), vec3(-1, 1, 1),Material(vec3(1, 0, 0)*0.5, vec3(1))),
    # Tri(vec3(1, 1, 1), vec3(1, -1, -1), vec3(1, -1, 1),Material(vec3(0, 1, 0)*0.5, vec3(1))),
    # Tri(vec3(1, -1, -1), vec3(1, 1, 1), vec3(1, 1, -1),Material(vec3(0, 1, 0)*0.5, vec3(1)))
]
def loadobj(path,Triangles):
    vertices=[]
    triangles=[]
    normals=[]
    with open(path, 'r') as f:
        for line in f:
            if line[0] == '#':
                continue
            pieces = line.split(' ')
            if pieces[0] == 'v':
                vertices.append([float(x) for x in pieces[1:4]])
            elif pieces[0] == 'f':
                if pieces[1] == '':
                    triangles.append([int(x.split('/')[0]) - 1 for x in pieces[2:]])
                else:
                    triangles.append([int(x.split('/')[0]) - 1 for x in pieces[1:]])
            elif pieces[0] == 'vn':
                normals.append([float(x) for x in pieces[1:]])
            else:
                pass
    print(vertices[5])
    for i in range(0, 200, 3):
        Triangles.append(Tri(v1=vertices[i],
                            v2=vertices[i+1],
                            v3=vertices[i+2],
                            material=Material(vec3(1, 1, 1) * 0.4, vec3(100))))



def init_surf_mesh(model_name, Tria):
    # 这是用来排列indices的taichi kernel函数
    @ti.kernel
    def init_surf_indices(mesh: ti.template(), indices: ti.template()):
        for f in mesh.faces:
            for j in ti.static(range(3)):
                indices[f.id * 3 + j] = f.verts[j].id

    # 1. 先加载模型
    # relation的意思: F是Face, V是Verts FV表示通过一个面可以找到它的三个顶点，
    # 要在加载mesh的时候就把这个关系建立好
    theMesh = Patcher.load_mesh(model_name, relations=["FV"])
    # 2. 定义x场，每个verts位置挂载位置x, 它是一个vec3
    theMesh.verts.place({'x': ti.math.vec3})
    # 3. 把numpy的数据传给x（一开始我们读的坐标是numpy的）
    theMesh.verts.x.from_numpy(theMesh.get_position_as_numpy())
    # 4. 定义一个indices场，这是为了后面渲染用的(scene.mesh), 它是一维数组，长度是面的数量*3
    # display_indices = ti.field(ti.i32, shape=len(theMesh.faces) * 3)
    # # 5. 按照每三个一组的顺序，把每个面的三个顶点的id传给indices
    # init_surf_indices(theMesh, display_indices)
    b = theMesh.get_position_as_numpy().flatten()
    print(len(b))



# 这里我们读入了bunny.ob
loadobj("../bunny/bunny.obj", Triangles)

objects_num = len(Triangles)  # 物体个数
objects = Tri.field(shape=objects_num)  # ...
for i in range(objects_num):
    objects[i] = Triangles[i]


@ti.dataclass
class HitRecord:
    object: Tri = objects[0]  # SDF物体
    position: vec3 = vec3(0, 0, 0)  # 位置
    distance: float = 1145141919.810
    hit: bool = False  # 是否击中


@ti.func
def calc_normal(tri: Tri, ray: Ray) -> vec3:  # representing the surface normal by the gradient of the SDF
    triangle_nor = normalize(cross(tri.v2 - tri.v1, tri.v3 - tri.v1))  # 求三角形的法向量
    if dot(triangle_nor, ray.direction) > 0.0:
        triangle_nor = -triangle_nor
    return triangle_nor


@ti.func
def intersect(tri: Tri, ray: Ray) -> HitRecord:
    isHit = True
    ray_ori = ray.origin
    ray_dre = ray.direction
    triangle_nor = normalize(cross(tri.v2 - tri.v1, tri.v3 - tri.v1))  # 求三角形的法向量
    # if (dot(N, d) > 0.0f) N = -N;
    if dot(triangle_nor, ray_dre) > 0.0:
        triangle_nor = -triangle_nor
        # 如果光线方向和三角形平行
    if abs(dot(triangle_nor, ray_dre)) < 0.00001:
        isHit = False
    t = (dot(triangle_nor, tri.v1) - dot(ray_ori, triangle_nor)) / dot(ray_dre, triangle_nor)
    # 如果三角形在相机背面
    if t < 0.0005:
        isHit = False
    # 计算光线与三角形所在平面的交点
    intersect_point = ray_ori + ray_dre * t
    # 判断交点是否在三角形中
    c1 = cross(tri.v2 - tri.v1, intersect_point - tri.v1)
    c2 = cross(tri.v3 - tri.v2, intersect_point - tri.v2)
    c3 = cross(tri.v1 - tri.v3, intersect_point - tri.v3)
    nor = normalize(cross(tri.v2 - tri.v1, tri.v3 - tri.v1))  # 原生法向量
    if (dot(c1, nor) < 0) or (dot(c2, nor) < 0) or (dot(c3, nor) < 0):
        isHit = False
    return HitRecord(tri, intersect_point, t, isHit)


@ti.func
def raycast(ray: Ray) -> HitRecord:  # ray marching to obtain the intersection with the surface
    record = HitRecord(distance=1145141919.810, hit=False)  # step a little off the surface first
    for i in range(objects_num):  # need a maximum number of steps
        r = intersect(objects[i], ray)
        if r.hit and r.distance < record.distance:
            record = r
    return record


@ti.func
def hemispheric_sampling(normal: vec3) -> vec3:  # choose a random direction in the normal hemisphere
    z = 2.0 * ti.random() - 1.0
    a = ti.random() * 2.0 * pi
    xy = sqrt(1.0 - z * z) * vec2(sin(a), cos(a))
    return normalize(normal + vec3(xy, z))


@ti.func
def raytrace(ray: Ray) -> Ray:  # Path Tracing
    for i in range(3):  # 3 times is already enough to bring Global Illumination to the scene
        inv_pdf = exp(float(i) / 128.0)
        roulette_prob = 1.0 - (1.0 / inv_pdf)  # Russian Roulette for spreading the computation between frames
        if ti.random() < roulette_prob: ray.color *= roulette_prob; break  # end of tracing

        record = raycast(ray)  # 计算光线是否与场景相交
        if not record.hit:
            ray.color = vec3(0)
            break  # 没有集中光源
        normal = calc_normal(record.object, ray)  # 计算光线击中点的法线
        ray.direction = hemispheric_sampling(normal)  # 随机重新生成方向
        ray.color *= record.object.material.albedo  # ray needs to be multiplied by the albedo
        ray.origin = record.position  # 光源更新
        # ==========================================图像处理=============================================================
        intensity = dot(ray.color, vec3(0.299, 0.587, 0.114))  # calculating the intensity of ray
        ray.color *= record.object.material.emission  # multiplying the emission
        visible = dot(ray.color, vec3(0.299, 0.587, 0.114))  # new brightness
        if intensity < visible or visible < 0.000001:
            break  # too dark or arrive at the light source
    return ray


@ti.kernel
def render(camera_position: vec3, camera_lookat: vec3, camera_up: vec3):
    for i, j in image_pixels:  # 遍历每个像素
        buffer = image_buffer[i, j]  # current buffer color
        # 相机
        z = normalize(camera_position - camera_lookat)
        x = normalize(cross(camera_up, z))  # calculating the camera coordinate system
        y = cross(z, x)
        # 相机
        half_width = half_height = tan(radians(35) * 0.5)  # calculate camera frame position and size
        lower_left_corner = camera_position - half_width * x - half_height * y - z
        horizontal = 2.0 * half_width * x
        vertical = 2.0 * half_height * y

        uv = (vec2(i, j) + vec2(ti.random(), ti.random())) / vec2(image_resolution)  # 超采样
        po = lower_left_corner + uv.x * horizontal + uv.y * vertical
        rd = normalize(po - camera_position)  # 计算相机发出的光线方向

        ray = raytrace(Ray(camera_position, rd, vec3(1)))  # 开始路径追踪
        buffer += vec4(ray.color, 1.0)  # accumulate colors and record the number of accumulations
        image_buffer[i, j] = buffer  # 更新缓存

        color = buffer.rgb / buffer.a  # calculate the average value of colors
        color = pow(color, vec3(1.0 / 2.2))  # Gamma correction, then use ACES tone mapping
        color = mat3(0.597190, 0.35458, 0.04823, 0.07600, 0.90834, 0.01566, 0.02840, 0.13383, 0.83777) @ color
        color = (color * (color + 0.024578) - 0.0000905) / (color * (0.983729 * color + 0.4329510) + 0.238081)
        color = mat3(1.60475, -0.531, -0.0736, -0.102, 1.10813, -0.00605, -0.00327, -0.07276, 1.07602) @ color
        image_pixels[i, j] = clamp(color, 0, 1)  # write pixels, clamp the brightness that cannot be displayed


def main():
    window = ti.ui.Window("Cornell Box", image_resolution)  # create window
    canvas = window.get_canvas()
    while window.running:  # main loop of the window
        render(vec3(0, 0, 3.5), vec3(0, 0, -1), vec3(0, 1, 0))  # 传入相机参数
        canvas.set_image(image_pixels)  # writing pixels to canvas
        window.show()  # continue to show window


if __name__ == '__main__':
    main()
