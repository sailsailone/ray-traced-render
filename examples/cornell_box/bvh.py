import taichi as ti


@ti.data_oriented
class BVHNode:
    def __init__(self):
        self.left = None
        self.right = None
        self.index = 0
        self.n = 0
        self.AA = ti.Vector.field(3, dtype=float, shape=())
        self.BB = ti.Vector.field(3, dtype=float, shape=())

@ti.func
def min3(x, y, z):
    return min(x, min(y, z))

@ti.func
def max3(x, y, z):
    return max(x, max(y, z))


@ti.func
def cmpx(a, b):
    return a.center.x < b.center.x


@ti.func
def cmpy(a, b):
    return a.center.y < b.center.y


@ti.func
def cmpz(a, b):
    return a.center.z < b.center.z


@ti.kernel
def build_BVH(triangles: ti.ext_arr(), l: int, r: int, n: int) -> BVHNode:
    if l > r:
        return None

    node = BVHNode()
    node.AA[None] = ti.Vector([1.145141919e9, 1.145141919e9, 1.145141919e9])
    node.BB[None] = ti.Vector([-1.145141919e9, -1.145141919e9, -1.145141919e9])

    # 计算 AABB
    for i in range(l, r + 1):
        # 最小点 AA
        minx = min3(triangles[i].p1.x, triangles[i].p2.x, triangles[i].p3.x)
        miny = min3(triangles[i].p1.y, triangles[i].p2.y, triangles[i].p3.y)
        minz = min3(triangles[i].p1.z, triangles[i].p2.z, triangles[i].p3.z)
        node.AA[None][0] = min(node.AA[None][0], minx)
        node.AA[None][1] = min(node.AA[None][1], miny)
        node.AA[None][2] = min(node.AA[None][2], minz)
        # 最大点 BB
        maxx = max3(triangles[i].p1.x, triangles[i].p2.x, triangles[i].p3.x)
        maxy = max3(triangles[i].p1.y, triangles[i].p2.y, triangles[i].p3.y)
        maxz = max3(triangles[i].p1.z, triangles[i].p2.z, triangles[i].p3.z)
        node.BB[None][0] = max(node.BB[None][0], maxx)
        node.BB[None][1] = max(node.BB[None][1], maxy)
        node.BB[None][2] = max(node.BB[None][2], maxz)

    # 不多于 n 个三角形 返回叶子节点
    if (r - l + 1) <= n:
        node.n = r - l + 1
        node.index = l
        return ti.static(node)

    lenx, leny, lenz = node.BB[None] - node.AA[None]
    # 按x划分
    if lenx >= leny and lenx >= lenz:
        ti.experimental.sort(triangles, lambda i: (triangles[i].p1[0] + triangles[i].p2[0] + triangles[i].p3[0]) / 3.0,
                             l, r + 1)
    # 按y划分
    elif leny >= lenx and leny >= lenz:
        ti.experimental.sort(triangles, lambda i: (triangles[i].p1[1] + triangles[i].p2[1] + triangles[i].p3[1]) / 3.0,
                             l, r + 1)
    # 按z划分
    else:
        ti.experimental.sort(triangles, lambda i: (triangles[i].p1[2] + triangles[i].p2[2] + triangles[i].p3[2]) / 3.0,
                             l, r + 1)

    mid = (l + r) // 2
    node.left = build_BVH(triangles, l, mid, n)
    node.right = build_BVH(triangles, mid + 1, r, n)
    return ti.static(node)


